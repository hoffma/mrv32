library ieee;
use ieee.std_logic_1164.all;

package tilelink is
    -- TL-UL opcodes
    -- TODO: TL-UL operation can be seen from page 46 in the spec
    -- https://www.sifive.com/document-file/tilelink-spec-1.9.3
    -- channel A
    constant OP_GET               : std_logic_vector(2 downto 0) := "100";
    constant OP_PUT_FULL_DATA     : std_logic_vector(2 downto 0) := "000";
    constant OP_PUT_PARTIAL_DATA  : std_logic_vector(2 downto 0) := "001";
    -- channel B
    constant OP_ACCESS_ACK_DATA   : std_logic_vector(2 downto 0) := "001";
    constant OP_ACCESS_ACK        : std_logic_vector(2 downto 0) := "000";

    constant tl_w : natural := 4; -- number of data bytes
    constant tl_a : natural := 32; -- address width in bit
    constant tl_z : natural := 2; -- 2^2 = 4 byte lanes == 32 bit data
    constant tl_o : natural := 1;
    constant tl_i : natural := 1;

    type tlul_a_t is record
        opcode    : std_logic_vector(2 downto 0);
        param     : std_logic_vector(2 downto 0);
        size      : std_logic_vector(tl_z-1 downto 0);
        source    : std_logic_vector(tl_o-1 downto 0);
        address   : std_logic_vector(tl_a-1 downto 0);
        mask      : std_logic_vector(tl_w-1 downto 0);
        data      : std_logic_vector((tl_w*8)-1 downto 0);
        corrupt   : std_logic;
        valid     : std_logic;
    end record tlul_a_t;

    type tlul_d_t is record
        opcode    : std_logic_vector(2 downto 0);
        param     : std_logic_vector(2 downto 0);
        size      : std_logic_vector(tl_z-1 downto 0);
        source    : std_logic_vector(tl_o-1 downto 0);
        sink      : std_logic_vector(tl_i-1 downto 0);
        denied    : std_logic;
        data      : std_logic_vector((tl_w*8)-1 downto 0);
        corrupt   : std_logic;
        valid     : std_logic;
    end record tlul_d_t;
end package tilelink;
