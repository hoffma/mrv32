library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tilelink.all;
use work.types.all;
-- simple wrapper architecture to enable von-neuman style bus
entity mrv32_tl is
    Generic (
        RESET_VECTOR: std_logic_vector(31 downto 0) := x"80000000"
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;
        ibus_tla : out tlul_a_t;
        ibus_a_ready : in std_logic;
        ibus_tld : in tlul_d_t;
        ibus_d_ready : out std_logic;

        dbus_tla : out tlul_a_t;
        dbus_a_ready : in std_logic;
        dbus_tld : in tlul_d_t;
        dbus_d_ready : out std_logic;

        int_tim   : in STD_LOGIC;
        int_ext   : in STD_LOGIC
    );
end mrv32_tl;

architecture behave of mrv32_tl is
    signal s_cpu : cpu_interface;
    signal prev_istrb : std_logic;
    signal prev_dstrb : std_logic;


    signal i_opcode : std_logic_vector(2 downto 0);
    signal d_opcode : std_logic_vector(2 downto 0);

    type state_t is (ST_WAIT_REQUEST, ST_RESPONSE);
    signal i_curr_state : state_t;
    signal d_curr_state : state_t;
begin

    i_opcode <= OP_GET when (s_cpu.i_strb = '1') else
                (others => 'U');

    d_opcode <= OP_GET when (s_cpu.d_wen = x"0" and s_cpu.d_strb = '1') else
              OP_PUT_FULL_DATA when (s_cpu.d_wen = x"f" and s_cpu.d_strb = '1') else
              OP_PUT_PARTIAL_DATA when (s_cpu.d_wen /= x"0" and s_cpu.d_strb = '1') else
              (others => 'U');

    -- TODO: resolve these, especially corrupt and denied (from chan D)
    ibus_tla.size <= "10";
    ibus_tla.param <= (others => 'U');
    ibus_tla.source <= "1";
    ibus_tla.corrupt <= '0';
    ibus_p: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                prev_istrb <= '0';
                s_cpu.i_rdata <= (others => '0');
                ibus_d_ready <= '0';
                ibus_tla.valid <= '0';
                ibus_tla.mask <= x"0";
                ibus_tla.data <= (others => 'U');
                ibus_tla.opcode <= "000";
                ibus_tla.address <= (others => 'U');
            else
                prev_istrb <= s_cpu.i_strb;
                ibus_tla.valid <= '0';
                ibus_tla.mask <= x"0";
                ibus_tla.data <= (others => 'U');
                ibus_tla.opcode <= "000";
                -- tl_m2s.a_address <= (others => 'U');
                s_cpu.i_rdata <= (others => 'U');
                s_cpu.i_valid <= '0';
                ibus_d_ready <= '1';

                case i_curr_state is
                    when ST_WAIT_REQUEST =>
                        if prev_istrb = '0' and s_cpu.i_strb = '1' then
                            ibus_tla.valid <= '1';
                            ibus_tla.opcode <= i_opcode;
                            ibus_tla.address <= s_cpu.i_addr;
                            -- no write on ibus
                            i_curr_state <= ST_RESPONSE;
                        end if;
                    when ST_RESPONSE =>
                        if ibus_a_ready = '1' and ibus_tld.valid = '1' then
                            s_cpu.i_rdata <= (others => '0');
                            s_cpu.i_valid <= '1';
                            if ibus_tld.opcode = OP_ACCESS_ACK_DATA then
                                s_cpu.i_rdata <= ibus_tld.data;
                                s_cpu.i_valid <= ibus_tld.valid;
                            elsif ibus_tld.opcode = OP_ACCESS_ACK_DATA then
                                s_cpu.i_valid <= ibus_tld.valid;
                            end if;
                            i_curr_state <= ST_WAIT_REQUEST;
                        end if;
                end case;
            end if;
        end if;
    end process;

    -- DBUS
    dbus_tla.size <= "10";
    dbus_tla.param <= (others => 'U');
    dbus_tla.source <= "1";
    dbus_tla.corrupt <= '0';
    dbus_p: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                prev_dstrb <= '0';
                s_cpu.d_rdata <= (others => '0');
                dbus_d_ready <= '0';
                dbus_tla.valid <= '0';
                dbus_tla.mask <= x"0";
                dbus_tla.data <= (others => 'U');
                dbus_tla.opcode <= "000";
                dbus_tla.address <= (others => 'U');
            else
                prev_dstrb <= s_cpu.d_strb;
                dbus_tla.valid <= '0';
                dbus_tla.mask <= x"0";
                dbus_tla.data <= (others => 'U');
                dbus_tla.opcode <= "000";
                -- tl_m2s.a_address <= (others => 'U');
                s_cpu.d_rdata <= (others => 'U');
                s_cpu.d_valid <= '0';
                dbus_d_ready <= '1';

                case d_curr_state is
                    when ST_WAIT_REQUEST =>
                        if prev_dstrb = '0' and s_cpu.d_strb = '1' then
                            dbus_tla.valid <= '1';
                            dbus_tla.opcode <= d_opcode;
                            dbus_tla.address <= s_cpu.d_addr;
                            dbus_tla.data <= s_cpu.d_wdata;
                            dbus_tla.mask <= s_cpu.d_wen;
                            d_curr_state <= ST_RESPONSE;
                        end if;
                    when ST_RESPONSE =>
                        if dbus_a_ready = '1' and dbus_tld.valid = '1' then
                            s_cpu.d_rdata <= (others => '0');
                            s_cpu.d_valid <= '1';
                            if dbus_tld.opcode = OP_ACCESS_ACK_DATA then
                                s_cpu.d_rdata <= dbus_tld.data;
                                s_cpu.d_valid <= dbus_tld.valid;
                            elsif dbus_tld.opcode = OP_ACCESS_ACK then
                                s_cpu.d_valid <= dbus_tld.valid;
                            end if;
                            d_curr_state <= ST_WAIT_REQUEST;
                        end if;
                end case;
            end if;
        end if;
    end process;

    inst_mrv_core : entity work.mrv32_core
    generic map (
        RESET_VECTOR => RESET_VECTOR
    ) port map (
        clk => clk,
        rst => rst,
        i_strb => s_cpu.i_strb,
        i_addr => s_cpu.i_addr,
        i_rdata => s_cpu.i_rdata,
        i_valid => s_cpu.i_valid,

        d_strb => s_cpu.d_strb,
        d_wen => s_cpu.d_wen,
        d_addr => s_cpu.d_addr,
        d_wdata => s_cpu.d_wdata,
        d_rdata => s_cpu.d_rdata,
        d_valid => s_cpu.d_valid,

        int_tim => int_tim,
        int_ext => int_ext
    );
end behave;

