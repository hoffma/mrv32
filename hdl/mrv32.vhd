-- MIT License
-- 
-- Copyright (c) 2024 Mario
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- tl;dr: https://files.catbox.moe/wrjabz.mp3
-- Auther: Mario Hoffmann

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.types.all;
-- simple wrapper architecture to enable von-neuman style bus
entity mrv32 is
    Generic (
        RESET_VECTOR : std_logic_vector(31 downto 0) := x"80000000"
    );
    Port (
        clk         : in STD_LOGIC;
        rst         : in STD_LOGIC;

        strb      : out std_logic;
        wen       : out std_logic_vector(3 downto 0);
        addr      : out std_logic_vector(31 downto 0);
        wdata     : out std_logic_vector(31 downto 0);
        rdata     : in std_logic_vector(31 downto 0);
        valid     : in std_logic;

        int_tim   : in STD_LOGIC;
        int_ext   : in STD_LOGIC
    );
end mrv32;

architecture behave of mrv32 is
    signal s_cpu : cpu_interface;
begin
    -- very simple decoder.
    -- TODO: introduce caches inside this wrapper
    process(s_cpu, rdata, valid)
    begin
        strb <= '0';
        addr <= (others => 'U');
        wen <= "0000";
        wdata <= (others => 'U');

        s_cpu.i_rdata <= (others => 'U');
        s_cpu.i_valid <= '0';

        s_cpu.d_rdata <= (others => 'U');
        s_cpu.d_valid <= '0';

        if s_cpu.i_strb = '1' then
            strb <= s_cpu.i_strb;
            addr <= s_cpu.i_addr;
            wen <= "0000";
            wdata <= (others => 'U');
            s_cpu.i_rdata <= rdata;
            s_cpu.i_valid <= valid;
        elsif s_cpu.d_strb = '1' then
            strb <= s_cpu.d_strb;
            addr <= s_cpu.d_addr;
            wen <= s_cpu.d_wen;
            wdata <= s_cpu.d_wdata;
            s_cpu.d_rdata <= rdata;
            s_cpu.d_valid <= valid;
        end if;
    end process;

    inst_mrv_core : entity work.mrv32_core
    generic map (
        RESET_VECTOR => x"8000_0000"
    ) port map (
        clk => clk,
        rst => rst,
        i_strb => s_cpu.i_strb,
        i_addr => s_cpu.i_addr,
        i_rdata => s_cpu.i_rdata,
        i_valid => s_cpu.i_valid,

        d_strb => s_cpu.d_strb,
        d_wen => s_cpu.d_wen,
        d_addr => s_cpu.d_addr,
        d_wdata => s_cpu.d_wdata,
        d_rdata => s_cpu.d_rdata,
        d_valid => s_cpu.d_valid,

        int_tim => int_tim,
        int_ext => int_ext 
    );
end behave;
