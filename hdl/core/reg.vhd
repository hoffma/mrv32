library ieee;
use ieee.std_logic_1164.all;

entity reg is
    Generic (
        REG_WIDTH : natural := 32
    );
    Port (
        clk : in std_logic;
        en : in std_logic;
        d : in std_logic_vector(REG_WIDTH-1 downto 0);
        q : out std_logic_vector(REG_WIDTH-1 downto 0)
    );
end reg;

architecture behave of reg is
begin
    process(clk)
    begin
        if rising_edge(clk) then
            if en = '1' then
                q <= d;
            end if;
        end if;
    end process;
end behave;
