-- MIT License
-- 
-- Copyright (c) 2024 Mario
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- tl;dr: https://files.catbox.moe/wrjabz.mp3
-- Auther: Mario Hoffmann
-- ###############################################################
-- ####################### PROCESSOR DESCRIPTION #################
-- ###############################################################
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.constants.all;
use work.types.all;

entity mrv32_core is
    Generic (
        RESET_VECTOR : std_logic_vector(31 downto 0) := x"80000000"
    );
    Port (
        clk         : in STD_LOGIC;
        rst         : in STD_LOGIC;

        i_strb      : out std_logic;
        i_addr      : out std_logic_vector(31 downto 0);
        i_rdata     : in std_logic_vector(31 downto 0);
        i_valid     : in std_logic;

        d_strb      : out std_logic;
        d_wen       : out std_logic_vector(3 downto 0);
        d_addr      : out std_logic_vector(31 downto 0);
        d_wdata     : out std_logic_vector(31 downto 0);
        d_rdata     : in std_logic_vector(31 downto 0);
        d_valid     : in std_logic;

        int_tim     : in std_logic;
        int_ext     : in std_logic
    );
end mrv32_core;

architecture Behavioral of mrv32_core is
    signal ifid_en : std_logic;
    signal idex_en : std_logic;
    signal exmem_en : std_logic;
    signal memwb_en : std_logic;

    signal pc_en : std_logic;
    signal curr_pc : std_logic_vector(31 downto 0);
    signal next_pc : std_logic_vector(31 downto 0);

    signal id_instr : std_logic_vector(31 downto 0);

    signal id_immediate : std_logic_vector(31 downto 0);
    signal id_rs1_addr : std_logic_vector(4 downto 0);
    signal id_rs2_addr : std_logic_vector(4 downto 0);
    signal id_rd_addr : std_logic_vector(4 downto 0);
    signal id_rd_wen : std_logic;
    signal id_alu_a_src : std_logic_vector(1 downto 0);
    signal id_alu_b_src : std_logic_vector(1 downto 0);
    signal id_alu_op : aluop_t;
    signal id_is_branch : std_logic;
    signal id_is_jump: std_logic;
    signal id_pc_add_src : std_logic;
    signal id_funct3 : std_logic_vector(2 downto 0);
    signal id_mem_write : std_logic;
    signal id_mem_read : std_logic;
    signal id_csr_op : csr_instr_t;
    signal id_csr_addr   : std_logic_vector(11 downto 0);
    -- signal id_mem_strb : std_logic;
    -- signal id_mem_instr : std_logic;
    signal id_istrb : std_logic;
    signal id_dstrb : std_logic;

    signal ex_jump_pc : std_logic_vector(31 downto 0);
    signal ex_immediate : std_logic_vector(31 downto 0);
    signal ex_rs1_data : std_logic_vector(31 downto 0);
    signal ex_rs2_data : std_logic_vector(31 downto 0);
    signal ex_alu_op : aluop_t;
    signal ex_branch_predicate : std_logic;

    signal mem_jump_pc : std_logic_vector(31 downto 0);
    -- signal mem_alu_y : std_logic_vector(31 downto 0);
    signal mem_mem_wdata : std_logic_vector(31 downto 0);

    signal wb_rd_data : std_logic_vector(31 downto 0);
    signal wb_rd_wen : std_logic;

    signal alu_y : std_logic_vector(31 downto 0);
    signal alu_y_reg : std_logic_vector(31 downto 0);
    signal alu_busy : std_logic;

    signal tmp_csr_out : std_logic_vector(31 downto 0);
    signal next_epc : std_logic_vector(31 downto 0);
    signal load_epc : std_logic;

    signal is_mret : std_logic;
    signal is_interrupt : std_logic;

    signal s_d_addr : std_logic_vector(31 downto 0);

    signal opcode_type : optype_t;
begin
-- ######### CONTROL UNIT ##########
    inst_cu : entity work.control_unit(new_behave)
    port map (
        clk => clk,
        rst => rst,

        opcode_type => opcode_type,
        interrupt => is_interrupt,
        istrb => id_istrb,
        dstrb => id_dstrb,
        i_valid => i_valid,
        d_valid => d_valid,
        alu_busy => alu_busy,
        ifid_en => ifid_en,
        idex_en => idex_en,
        exmem_en => exmem_en,
        memwb_en => memwb_en,

        pc_en => pc_en
    );
-- #################################

-- ######## MEM BUS CONTROL ########
    i_strb <= id_istrb;
    d_strb <= id_dstrb;
    i_addr <= curr_pc;
    d_addr <= s_d_addr;
    s_d_addr <= alu_y_reg when (id_dstrb = '1') else
                (others => '0');
                -- alu_y_reg when (memwb_en = '1') and (id_mem_read = '1' or id_mem_write = '1') else
-- #################################

-- ####### PROGRAM COUNTER #########
    inst_pc_reg : entity work.reg
    generic map (
        REG_WIDTH => 32
    ) port map (
        clk => clk,
        -- rst => rst,
        en => pc_en,
        d => next_pc,
        q => curr_pc
    );
-- #################################

-- ########### CSR BLOCK ###########
    inst_csr : entity work.csr
    port map (
        clk => clk,
        rst => rst,
        pc_en => pc_en,
        pc_in => curr_pc,
        csr_in => alu_y_reg,
        csr_addr => id_csr_addr,
        csr_op => id_csr_op,
        int_ext => int_ext,
        int_tim => int_tim,
        is_mret => is_mret,
        csr_out => tmp_csr_out,
        is_interrupt => is_interrupt,
        load_epc => load_epc,
        epc_out => next_epc
    );
-- #################################

-- ########### IF STAGE ############
    process(clk)
    begin
        if rising_edge(clk) then
            if ifid_en = '1' then
                id_instr <= i_rdata;
            end if;
        end if;
    end process;
-- #################################

-- ########### ID STAGE ############
    id_block : block is
        signal tmp_rs1_data : std_logic_vector(31 downto 0);
        signal tmp_rs2_data : std_logic_vector(31 downto 0);
    begin
        inst_idecode : entity work.idecode
        port map (
            instruction => id_instr,

            opcode_type => opcode_type,
            immediate => id_immediate,
            rs1_addr => id_rs1_addr,
            rs2_addr => id_rs2_addr,
            rd_addr => id_rd_addr,
            rd_wen => id_rd_wen,
            alu_a_src => id_alu_a_src,
            alu_b_src => id_alu_b_src,
            alu_op => id_alu_op,
            is_jump => id_is_jump,
            is_branch => id_is_branch,
            pc_add_src => id_pc_add_src,
            funct3 => id_funct3,
            mem_write => id_mem_write,
            mem_read => id_mem_read,
            csr_op => id_csr_op,
            csr_addr => id_csr_addr
        );

        inst_regfile : entity work.regfile(reg_gen)
        generic map (
            REG_WIDTH => 32,
            REG_COUNT => 32
        ) port map (
            clk => clk,
            r1_addr => id_rs1_addr,
            r2_addr => id_rs2_addr,
            wr_addr => id_rd_addr,
            wr_data => wb_rd_data,
            wr_en => wb_rd_wen,
            r1_data => tmp_rs1_data,
            r2_data => tmp_rs2_data
        );

        process(clk)
        begin
            if rising_edge(clk) then
                if idex_en = '1' then
                    ex_rs1_data <= tmp_rs1_data;
                    ex_rs2_data <= tmp_rs2_data;
                    ex_immediate <= id_immediate;
                end if;
            end if;
        end process;
    end block id_block;
-- #################################

-- ########### EX STAGE ############
    ex_block : block is
        signal alu_a : std_logic_vector(31 downto 0);
        signal alu_b : std_logic_vector(31 downto 0);
        signal ex_alu_lt : std_logic;
        signal ex_alu_ltu : std_logic;
        signal ex_alu_eq : std_logic;
        signal branch_eval : std_logic;
    begin
        with id_alu_a_src select
            alu_a <= ex_rs1_data when "00",
                     curr_pc when "01",
                     (others => '0') when others;

        with id_alu_b_src select
            alu_b <= ex_rs2_data when "00",
                     ex_immediate when "01",
                     std_logic_vector(to_unsigned(4, alu_b'length)) when "10",
                     (others => '0') when others;

        process(clk)
        begin
            if rising_edge(clk) then
                if idex_en = '1' then
                    ex_alu_op <= id_alu_op;
                end if;
            end if;
        end process;

        inst_alu : entity work.alu
        port map (
            a => alu_a,
            b => alu_b,
            alu_op => ex_alu_op,
            busy => alu_busy,
            y => alu_y,
            lt => ex_alu_lt,
            ltu => ex_alu_ltu,
            eq => ex_alu_eq
        );

        alu_y_reg <= alu_y when rising_edge(clk);

        -- Calculate next pc
        with id_pc_add_src select
            ex_jump_pc <= std_logic_vector(signed(ex_rs1_data) + signed(ex_immediate)) when '1',
                       std_logic_vector(signed(curr_pc) + signed(ex_immediate)) when others;
        
        -- calculate branch predicate
        with id_alu_op select
            branch_eval <= (ex_alu_eq) when ALU_BEQ,
                           (not ex_alu_eq) when ALU_BNE,
                           (ex_alu_lt) when ALU_BLT,
                           (not ex_alu_lt) when ALU_BGE,
                           (ex_alu_ltu) when ALU_BLTU,
                           (not ex_alu_ltu) when ALU_BGEU,
                           '0' when others;

        ex_branch_predicate <= branch_eval and id_is_branch;

        process(clk)
        begin
            if rising_edge(clk) then
                -- mem_mem_wdata <= (others => '0');

                -- if exmem_en = '1' and id_mem_write = '1' then
                if exmem_en = '1' then
                    mem_mem_wdata <= ex_rs2_data;
                    mem_jump_pc <= ex_jump_pc;
                end if;
            end if;
        end process;
    end block ex_block;
-- #################################

-- ########### MEM STAGE ###########
    mem_block : block is
        signal mem_rd_data : std_logic_vector(31 downto 0);
        signal tmp_mask : std_logic_vector(3 downto 0);
        signal load_byte : std_logic_vector(7 downto 0);
        signal load_hw : std_logic_vector(15 downto 0);
        signal load_data : std_logic_vector(31 downto 0);
        signal tmp_wdata : std_logic_vector(31 downto 0);
        signal mask_decode : std_logic_vector(4 downto 0);
    begin
        process(tmp_mask, mem_mem_wdata)
        begin
            tmp_wdata <= (others => '0');
            case tmp_mask is
                when "0001" =>
                    tmp_wdata(7 downto 0) <= mem_mem_wdata(7 downto 0);
                when "0010" =>
                    tmp_wdata(15 downto 8) <= mem_mem_wdata(7 downto 0);
                when "0100" =>
                    tmp_wdata(23 downto 16) <= mem_mem_wdata(7 downto 0);
                when "1000" =>
                    tmp_wdata(31 downto 24) <= mem_mem_wdata(7 downto 0);
                when "0011" =>
                    tmp_wdata(15 downto 0) <= mem_mem_wdata(15 downto 0);
                when "1100" =>
                    tmp_wdata(31 downto 16) <= mem_mem_wdata(15 downto 0);
                when "1111" =>
                    tmp_wdata <= mem_mem_wdata;
                when others =>
            end case;
        end process;

        d_wdata <= tmp_wdata when (id_mem_write = '1') else (others => '0');

        mask_decode <= id_funct3&s_d_addr(1 downto 0);
        with mask_decode select
            tmp_mask <= "0001" when "00000",
                        "0010" when "00001",
                        "0100" when "00010",
                        "1000" when "00011",
                        "0011" when "00100",
                        "1100" when "00110",
                        "1111" when "01000",
                        "0000" when others;

        with s_d_addr(1) select
            load_hw <= d_rdata(15 downto 0) when '0',
                       d_rdata(31 downto 16) when '1',
                       (others => '0') when others;

        with s_d_addr(0) select
            load_byte <= load_hw(7 downto 0) when '0',
                         load_hw(15 downto 8) when '1',
                         (others => '0') when others;

        -- 000: load byte
        -- 001: load half word
        -- 010: load word
        -- 100: load byte unsigned
        -- 101: load half word unsigned
        with id_funct3 select
            load_data <= (31 downto 8 => load_byte(7)) & load_byte when "000",
                         (31 downto 16 => load_hw(15)) & load_hw when "001",
                         d_rdata when "010",
                         (31 downto 8 => '0') & load_byte when "100",
                         (31 downto 16 => '0') & load_hw when "101",
                         (others => '0') when others;

        mem_rd_data <= load_data when (id_mem_read = '1') else
                       tmp_csr_out when (id_csr_op /= CSR_NOP) else
                       alu_y_reg;


        next_pc <= RESET_VECTOR when rst = '1' else
                   next_epc when load_epc = '1' else
                   next_epc when is_mret = '1' else
                   mem_jump_pc when ex_branch_predicate = '1' else
                   mem_jump_pc when id_is_jump = '1' else
                   std_logic_vector(signed(curr_pc) + 4);

        d_wen <= tmp_mask when (id_mem_write = '1') and (memwb_en = '1') else (others => '0');
        process(clk)
        begin
            if rising_edge(clk) then
                wb_rd_wen <= '0';
                wb_rd_data <= (others => '0');

                if memwb_en = '1' then
                    wb_rd_wen <= id_rd_wen;
                    wb_rd_data <= mem_rd_data;
                end if;
            end if;
        end process;
    end block mem_block;
-- #################################

-- ########### WB STAGE ############
    wb_block : block is
    begin
    end block wb_block;
-- #################################


end Behavioral;
