library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package TYPES is
    type optype_t is (OP_LUI, OP_AUIPC, OP_JAL, OP_JALR, OP_BRANCH, OP_LOAD,
                        OP_STORE, OP_IMM, OP_REG_REG, OP_FENCE, OP_SYSTEM, OP_ERR);

    type aluop_t is (
        ALU_ADD, ALU_SUB,
        ALU_SLT, ALU_SLTU,
        ALU_XOR, ALU_OR, ALU_AND,
        ALU_SLL, ALU_SRL, ALU_SRA,
        ALU_BEQ, ALU_BNE, ALU_BLT, ALU_BGE, ALU_BLTU, ALU_BGEU
    );

    subtype csr_addr_t is std_logic_vector(11 downto 0);
    type csr_instr_t is (CSR_NOP, CSRRW_WR, CSRRW, 
                        CSRRS_RD, CSRRS, 
                        CSRRC_RD, CSRRC);

    type csr_reg_name_t is (
        M_NONE,
        M_CYCLE,
        M_CYCLE_H,
        M_STATUS,
        M_ISA,
        M_IE,
        M_TVEC,
        M_SCRATCH,
        M_EPC,
        M_CAUSE,
        M_TVAL,
        M_IP,
        M_VENDORID,
        M_ARCHID,
        M_IMPID,
        M_HARTID
    );

    type csr_reg_t is record
        ro : boolean;
        name : csr_reg_name_t;
        addr : std_logic_vector(11 downto 0);
        val : std_logic_vector(31 downto 0);
    end record csr_reg_t;

    type csr_reg_arr_t is array(csr_reg_name_t) of csr_reg_t;

    type cpu_interface is record
        i_strb      : std_logic;
        i_addr      : std_logic_vector(31 downto 0);
        i_rdata     : std_logic_vector(31 downto 0);
        i_valid     : std_logic;

        d_strb      : std_logic;
        d_wen       : std_logic_vector(3 downto 0);
        d_addr      : std_logic_vector(31 downto 0);
        d_wdata     : std_logic_vector(31 downto 0);
        d_rdata     : std_logic_vector(31 downto 0);
        d_valid     : std_logic;
    end record cpu_interface;

    type cpu_simple is record
        strb      : std_logic;
        wen       : std_logic_vector(3 downto 0);
        addr      : std_logic_vector(31 downto 0);
        wdata     : std_logic_vector(31 downto 0);
        rdata     : std_logic_vector(31 downto 0);
        valid     : std_logic;
    end record cpu_simple;
end package TYPES;

package body TYPES is
end package body TYPES;
