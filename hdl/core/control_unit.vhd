library ieee;
use ieee.std_logic_1164.all;

library work;
use work.types.all;

entity control_unit is
    Port (
        clk : in std_logic;
        rst : in std_logic;

        opcode_type : in optype_t;

        interrupt   : in std_logic;

        istrb       : out std_logic;
        dstrb       : out std_logic;
        i_valid       : in std_logic;
        d_valid       : in std_logic;

        alu_busy    : in std_logic;

        ifid_en     : out std_logic;
        idex_en     : out std_logic;
        exmem_en    : out std_logic;
        memwb_en    : out std_logic;

        pc_en       : out std_logic
    );
end control_unit;

architecture new_behave of control_unit is
    type state_t is (STATE_START, STATE_IF, STATE_ID, STATE_EX, STATE_MEM, STATE_WB);
    signal curr_state : state_t;
    signal next_state : state_t;

    constant NOP_INSTR : std_logic_vector(31 downto 0) := x"00000013";
begin

    process(curr_state, interrupt, i_valid, d_valid, alu_busy, opcode_type, rst)
    begin
        next_state <= curr_state;
        istrb <= '0';
        dstrb <= '0';

        ifid_en <= '0';
        idex_en <= '0';
        exmem_en <= '0';
        memwb_en <= '0';
        pc_en <= '0';

        case curr_state is
            when STATE_START =>
                istrb <= '1';
                next_state <= STATE_IF;
            when STATE_IF =>
                istrb <= '1';
                ifid_en <= '1';
                if i_valid= '1' then
                    next_state <= STATE_ID;
                end if;
            when STATE_ID =>
                idex_en <= '1';
                next_state <= STATE_EX;
                if interrupt = '1' then
                    pc_en <= '1';
                    next_state <= STATE_IF;
                end if;
            when STATE_EX =>
                exmem_en <= '1';
                if alu_busy = '0' then
                    next_state <= STATE_MEM;
                end if;
                -- if (opcode_type = OP_LOAD) or (opcode_type = OP_STORE) then
                --     dstrb <= '1';
                -- end if;
            when STATE_MEM =>
                memwb_en <= '1';
                if (opcode_type = OP_LOAD) or (opcode_type = OP_STORE) then
                    dstrb <= '1';
                    if d_valid = '1' then
                        pc_en <= '1';
                        next_state <= STATE_WB;
                    end if;
                else
                    pc_en <= '1';
                    next_state <= STATE_WB;
                end if;
            when STATE_WB =>
                istrb <= '1';
                next_state <= STATE_IF;
        end case;

        if rst = '1' then
            pc_en <= '1';
            next_state <= STATE_START;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            curr_state <= next_state;

            if rst = '1' then
                curr_state <= STATE_START;
            end if;
        end if;
    end process;
end new_behave;
