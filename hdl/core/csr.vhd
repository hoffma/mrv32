library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.types.all;

entity csr is
    Port (
        clk : in std_logic;
        rst : in std_logic;

        pc_en : in std_logic;
        pc_in : in std_logic_vector(31 downto 0);

        csr_in : in std_logic_vector(31 downto 0);
        csr_addr : in std_logic_vector(11 downto 0);
        csr_op : in csr_instr_t;

        int_ext : in std_logic;
        int_tim : in std_logic;

        is_mret : out std_logic;

        csr_out : out std_logic_vector(31 downto 0);
        is_interrupt : out std_logic;
        load_epc : out std_logic;
        epc_out : out std_logic_vector(31 downto 0)
    );
end csr;

    -- TODO (for now):
    -- M_CYCLE
    -- M_STATUS(MPP:[12:11], MPIE:7, MIE:3, others => 0)
    -- M_IE(MEIP:11, MTIP:7, MSIP:3, others => 0) (interrupt enable)
    -- M_TVEC (trap handler address)
    -- M_EPC (exception program counter; where the exception occured)
    -- M_CAUSE
    -- M_IP(MEIP:11, MTIP:7, MSIP:3, others => 0) (interrupt pending)

    -- 1) check if exception/interrupt active
    -- 2) if yes, write current status to registers
    -- 3) if CSR op, execute that
    -- 4) else do nothing
    -- 5) ?????
    -- 6) Profit

    -- Instruction Fetch: Check if is exception (misaligned, access fault)
architecture behave of csr is
    signal m_cycle_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_cycleh_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_status_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_ie_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_tvec_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_epc_reg : std_logic_vector(31 downto 0) := (others => '0');
    signal m_cause_reg : std_logic_vector(31 downto 0) := (others => '0');

    signal reg_sel : csr_reg_name_t := M_NONE;

    signal csr_curr_val : std_logic_vector(31 downto 0);
    signal csr_new_val : std_logic_vector(31 downto 0);

    signal s_mret : std_logic;
begin
    -- TODO: The execution environment should provide a means to determine the
    -- current rate (cycles/second) at which the cycle counter is incrementing.
    -- risc-v spec p.59

    is_mret <= s_mret; 
    s_mret <= '1' when (csr_op = CSR_NOP) and (csr_addr = x"302") else '0';

    with csr_addr select
        reg_sel <= M_CYCLE when x"b00",
                   M_CYCLE_H when x"b80",
                   M_STATUS when x"300",
                   M_IE when x"304",
                   M_TVEC when x"305",
                   M_EPC when x"341",
                   M_CAUSE when x"342",
                   M_NONE when others;

    with reg_sel select
        csr_curr_val <= m_cycle_reg when M_CYCLE,
                        m_cycleh_reg when M_CYCLE_H,
                        m_status_reg when M_STATUS,
                        m_ie_reg when M_IE,
                        m_cause_reg when M_CAUSE,
                        (others => '0') when others;
    csr_out <= csr_curr_val when rising_edge(clk);

    csr_calc : process(csr_op, csr_in, csr_curr_val)
    begin
        csr_new_val <= (others => '0');
        case csr_op is
            when CSRRW|CSRRW_WR =>
                csr_new_val <= csr_in;
            when CSRRS|CSRRS_RD =>
                csr_new_val <= csr_curr_val or csr_in;
            when CSRRC|CSRRC_RD =>
                csr_new_val <= csr_curr_val and (not csr_in);
            when CSR_NOP =>
        end case;
    end process;

    csr_process : process(clk)
        variable excpt_cause : std_logic_vector(31 downto 0);
        variable saved_int_tim : std_logic;
        variable saved_int_ext : std_logic;
        variable tmp_cycle : std_logic_vector(63 downto 0);
        variable v_interrupt : std_logic;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                saved_int_ext := '0';
                saved_int_tim := '0';
                is_interrupt <= '0';
                epc_out <= (others => '0');
                load_epc <= '0';
                -- reset CSR
                -- m_cycle_reg <= (others => '0');
                -- m_cycleh_reg <= (others => '0');
                -- m_status_reg <= (others => '0');
                -- m_ie_reg <= (others => '0');
                -- m_cause_reg <= (others => '0');
            else
                v_interrupt := '0';
                saved_int_ext := saved_int_ext or (int_ext and m_ie_reg(11));
                saved_int_tim := saved_int_tim or (int_tim and m_ie_reg(7));

                if (m_status_reg(3) = '1') and (saved_int_ext = '1' or saved_int_tim = '1') then
                    v_interrupt := '1';
                end if;
                is_interrupt <= v_interrupt;

                -- increment cycle counter
                excpt_cause := (others => '0');
                tmp_cycle := std_logic_vector(unsigned(m_cycleh_reg & m_cycle_reg) + 1);
                -- m_cycle_reg <= std_logic_vector(unsigned(m_cycle_reg) + 1);
                m_cycleh_reg <= tmp_cycle(63 downto 32);
                m_cycle_reg <= tmp_cycle(31 downto 0);

                if pc_en = '1' then
                    load_epc <= '0';
                end if;

                if v_interrupt = '1' then
                    -- save previous interrupt enable
                    m_status_reg(7) <= '1';
                    -- clear interrupt enable
                    m_status_reg(3) <= '0';
                    -- save pc where interrupt occured
                    m_epc_reg <= pc_in;

                    if saved_int_tim = '1' then
                        excpt_cause := std_logic_vector(to_unsigned(16#7#, excpt_cause'length));
                    elsif saved_int_ext = '1' then
                        excpt_cause := std_logic_vector(to_unsigned(16#b#, excpt_cause'length));
                    end if;
                    excpt_cause(31) := '1';

                    m_cause_reg <= excpt_cause;
                    -- set pc to trap handler
                    epc_out <= m_tvec_reg;
                    load_epc <= '1';
                elsif s_mret = '1' then
                    -- restore previous interrupt enable
                    m_status_reg(3) <= m_status_reg(7);
                    -- clear previous interrupt enable
                    m_status_reg(7) <= '1';
                    m_cause_reg <= (others => '0');
                    -- set pc to epc
                    epc_out <= m_epc_reg;
                    saved_int_tim := '0';
                    saved_int_ext := '0';
                elsif (csr_op /= CSRRS_RD) and (csr_op /= CSRRC_RD) and (csr_op /= CSR_NOP) then
                    case reg_sel is
                        when M_CYCLE =>
                            m_cycle_reg <= csr_new_val;
                        when M_CYCLE_H =>
                            m_cycleh_reg <= csr_new_val;
                        when M_STATUS =>
                            m_status_reg <= csr_new_val;
                        when M_IE =>
                            m_ie_reg <= csr_new_val;
                        when M_TVEC =>
                            m_tvec_reg <= csr_new_val;
                        when M_EPC =>
                            m_epc_reg <= csr_new_val;
                        when M_CAUSE =>
                            m_cause_reg <= csr_new_val;
                        when others =>
                            Null;
                    end case;
                end if;
            end if;
        end if;
    end process;
end behave;
