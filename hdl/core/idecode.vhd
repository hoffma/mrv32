library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;
use work.types.all;

entity idecode is
    Port (
        instruction : in std_logic_vector(31 downto 0);
        
        opcode_type : out optype_t;
        
        rs1_addr    : out std_logic_vector(4 downto 0);
        rs2_addr    : out std_logic_vector(4 downto 0);
        rd_addr     : out std_logic_vector(4 downto 0);
        rd_wen      : out std_logic;

        immediate   : out std_logic_vector(31 downto 0);
        alu_a_src   : out std_logic_vector(1 downto 0);
        alu_b_src   : out std_logic_vector(1 downto 0);
        alu_op      : out aluop_t;
        is_branch   : out std_logic;
        is_jump     : out std_logic;
        pc_add_src  : out std_logic;

        funct3      : out std_logic_vector(2 downto 0);
        mem_write   : out std_logic;
        mem_read    : out std_logic;

        -- csr_rw      : out std_logic_vector(1 downto 0);
        csr_op      : out csr_instr_t;
        csr_addr    : out std_logic_vector(11 downto 0)
    );
end idecode;

architecture behave of idecode is
    constant C_LUI     : std_logic_vector(4 downto 0) := "01101";
    constant C_AUIPC   : std_logic_vector(4 downto 0) := "00101";
    constant C_JAL     : std_logic_vector(4 downto 0) := "11011";
    constant C_JALR    : std_logic_vector(4 downto 0) := "11001";
    constant C_BRANCH  : std_logic_vector(4 downto 0) := "11000";
    constant C_LOAD    : std_logic_vector(4 downto 0) := "00000";
    constant C_STORE   : std_logic_vector(4 downto 0) := "01000";
    constant C_IMM     : std_logic_vector(4 downto 0) := "00100";
    constant C_REG_REG : std_logic_vector(4 downto 0) := "01100";
    constant C_FENCE   : std_logic_vector(4 downto 0) := "00011";
    constant C_SYSTEM  : std_logic_vector(4 downto 0) := "11100";

    signal opcode_type_i : optype_t;
    signal csr_rd_wen : std_logic;

    signal rs1_addr_i : std_logic_vector(4 downto 0);
    signal rd_addr_i : std_logic_vector(4 downto 0);
    signal immediate_i : std_logic_vector(31 downto 0);
begin
    -- output signals
    opcode_type <= opcode_type_i;
    immediate <= immediate_i;
    rs1_addr <= rs1_addr_i;
    rd_addr <= rd_addr_i;
    funct3 <= instruction(14 downto 12);

    with instruction(6 downto 2) select opcode_type_i <=
        OP_LUI when C_LUI,
        OP_AUIPC when C_AUIPC,
        OP_JAL when C_JAL,
        OP_JALR when C_JALR,
        OP_BRANCH when C_BRANCH,
        OP_LOAD when C_LOAD,
        OP_STORE when C_STORE,
        OP_IMM when C_IMM,
        OP_REG_REG when C_REG_REG,
        OP_FENCE when C_FENCE,
        OP_SYSTEM when C_SYSTEM,
        OP_ERR when others;

    is_branch <= '1' when opcode_type_i = OP_BRANCH else '0';
    is_jump <= '1' when (opcode_type_i = OP_JAL) or (opcode_type_i = OP_JALR) else '0';
    
    imm_sel : process(opcode_type_i, instruction)
        -- immediates for each format
        -- no need for unsigned immediate, because:
        -- i.e., the immediate is first sign-extended to XLEN bits then treated as an unsigned number
        -- p.18, risc-v spec
        variable immI : std_logic_vector(31 downto 0);
        variable immS : std_logic_vector(31 downto 0);
        variable immB : std_logic_vector(31 downto 0);
        variable immU : std_logic_vector(31 downto 0);
        variable immJ : std_logic_vector(31 downto 0);
        variable opcode : std_logic_vector(4 downto 0);
    begin
        opcode := instruction(6 downto 2);

        immI := (31 downto 11 => instruction(31)) &
                instruction(30 downto 25) &
                instruction(24 downto 21) &
                instruction(20);

        immS := (31 downto 11 => instruction(31)) &
                instruction(30 downto 25) &
                instruction(11 downto 8) &
                instruction(7);

        immB := (31 downto 12 => instruction(31)) &
                instruction(7) &
                instruction(30 downto 25) &
                instruction(11 downto 8) &
                '0';

        immU := instruction(31) & 
                instruction(30 downto 20) &
                instruction(19 downto 12) &
                (11 downto 0 => '0');

        immJ := (31 downto 20 => instruction(31)) & 
                instruction(19 downto 12) &
                instruction(20) &
                instruction(30 downto 25) &
                instruction(24 downto 21) &
                '0';

        case opcode_type_i is
            when OP_LUI | OP_AUIPC =>
                immediate_i <= immU;
            when OP_JAL =>
                immediate_i <= immJ;
            when OP_JALR | OP_LOAD | OP_IMM | OP_SYSTEM =>
                immediate_i <= immI;
            when OP_BRANCH =>
                immediate_i <= immB;
            when OP_STORE =>
                immediate_i <= immS;
            when others =>
                immediate_i <= (others => '0');
        end case;

        -- with opcode_type_i select immediate <= 
        --     immU when OP_LUI, -- LUI
        --     immU when OP_AUIPC, -- AUIPC
        --     immJ when OP_JAL, -- JAL
        --     immI when OP_JALR, -- JALR
        --     immB when OP_BRANCH, -- Branch
        --     immI when OP_LOAD, -- Load
        --     immS when OP_STORE, -- Store
        --     immI when OP_IMM, -- ALUimm
        --     (others => '0') when others;
    end process;

    -- flag for possible PC computation
    -- 0 Branch and JAL (PC+immediate)
    -- 1 JALR (PC=rs1+immediate)
    with opcode_type_i select
        pc_add_src <= '1' when OP_JALR, '0' when others;

-- ######### REG DECODE ############
    reg_decode : block is
    begin
        with opcode_type_i select
            rs1_addr_i <= instruction(19 downto 15) when OP_REG_REG|OP_IMM|OP_LOAD|OP_STORE|OP_BRANCH|OP_FENCE|OP_JALR|OP_SYSTEM,
                          (others => '0') when others;

        with opcode_type_i select
            rs2_addr <= instruction(24 downto 20) when OP_REG_REG|OP_BRANCH|OP_STORE,
                        (others => '0') when others;

        with opcode_type_i select
            rd_addr_i <= instruction(11 downto 7) when OP_LUI|OP_AUIPC|OP_JAL|OP_JALR,
                         instruction(11 downto 7) when OP_LOAD|OP_REG_REG|OP_IMM,
                         instruction(11 downto 7) when OP_SYSTEM,
                         (others => '0') when others;

        with opcode_type_i select
            rd_wen <= '1' when OP_LUI|OP_AUIPC|OP_JAL|OP_JALR|OP_LOAD|OP_REG_REG|OP_IMM,
                      csr_rd_wen when OP_SYSTEM,
                      '0' when others;
    end block reg_decode;
-- #################################

-- ######### ALU DECODE ############
    alu_decode_block : block is
       signal funct7_i : std_logic_vector(6 downto 0);
       signal funct3_i : std_logic_vector(2 downto 0);
    begin
        funct3_i      <= instruction(14 downto 12);
        funct7_i      <= instruction(31 downto 25);

        -- alu a port input select
        -- 00 => rs1 data
        -- 01 => current PC
        -- 10 => 0
        -- 11 => *unused*
        with opcode_type_i select
            alu_a_src <= "00" when OP_REG_REG|OP_IMM|OP_LOAD|OP_STORE,
                         "01" when OP_JAL|OP_JALR|OP_AUIPC,
                         "10" when OP_LUI,
                         "00" when others;

        -- alu b port input select
        -- 00 => rs2 data
        -- 01 => immediate
        -- 10 => 4
        -- 11 => *unused*
        with opcode_type_i select 
            alu_b_src <= "00" when OP_REG_REG,
                         "01" when OP_IMM|OP_LOAD|OP_STORE|OP_AUIPC|OP_LUI,
                         "10" when OP_JAL|OP_JALR,
                         "00" when others;
        
        alu_decode : process(opcode_type_i, funct3_i, funct7_i)
        begin
            alu_op <= ALU_ADD;
            case opcode_type_i is
            when OP_REG_REG | OP_IMM =>
                case funct3_i is
                    when "000" =>
                        if opcode_type_i = OP_REG_REG and funct7_i(5) = '1' then
                            alu_op <= ALU_SUB;
                        else
                            alu_op <= ALU_ADD;
                        end if;
                    when "001" =>
                        alu_op <= ALU_SLL;
                    when "010" =>
                        alu_op <= ALU_SLT;
                    when "011" =>
                        alu_op <= ALU_SLTU;
                    when "100" =>
                        alu_op <= ALU_XOR;
                    when "101" =>
                        if funct7_i(5) = '1' then
                            alu_op <= ALU_SRA;
                        else
                            alu_op <= ALU_SRL;
                        end if;
                    when "110" =>
                        alu_op <= ALU_OR;
                    when "111" =>
                        alu_op <= ALU_AND;
                    when others =>
                        alu_op <= ALU_ADD;
                end case;
            -- /when OP_REG_REG | OP_IMM 

            when OP_BRANCH =>
                -- alu_op <= "10000" & funct3_i;
                case funct3_i is
                    when "000" =>
                        alu_op <= ALU_BEQ;
                    when "001" =>
                        alu_op <= ALU_BNE;
                    when "100" =>
                        alu_op <= ALU_BLT;
                    when "101" =>
                        alu_op <= ALU_BGE;
                    when "110" =>
                        alu_op <= ALU_BLTU;
                    when "111" =>
                        alu_op <= ALU_BGEU;
                    when others =>
                        alu_op <= ALU_ADD;
                end case;
            -- /when OP_BRANCH

            when OP_STORE | OP_LOAD =>
                alu_op <= ALU_ADD;
            -- /when OP_STORE | OP_LOAD =>

            when OTHERS =>
                alu_op <= ALU_ADD;
            end case;
        end process;
    end block alu_decode_block;
-- #################################

-- ######### MEM DECODE ############
    mem_decode_block : block is
       signal funct3_i : std_logic_vector(2 downto 0);
    begin
        funct3_i <= instruction(14 downto 12);
        mem_write <= '1' when opcode_type_i = OP_STORE else '0';
        mem_read <= '1' when opcode_type_i = OP_LOAD else '0';
    end block mem_decode_block;
-- #################################

-- ######### CSR DECODE ############
    csr_decode_block : block is
        constant CSRRW_T  : std_logic_vector(2 downto 0) := "001";
        constant CSRRS_T  : std_logic_vector(2 downto 0) := "010";
        constant CSRRC_T  : std_logic_vector(2 downto 0) := "011";
        constant CSRRWI_T : std_logic_vector(2 downto 0) := "101";
        constant CSRRSI_T : std_logic_vector(2 downto 0) := "110";
        constant CSRRCI_T : std_logic_vector(2 downto 0) := "111";

       signal funct3_i : std_logic_vector(2 downto 0);
       signal r1_addr : std_logic_vector(4 downto 0);
       signal csr_op_i : csr_instr_t;
    begin
        funct3_i <= instruction(14 downto 12);
        r1_addr <= instruction(19 downto 15);

        csr_op <= csr_op_i when (opcode_type_i = OP_SYSTEM) else CSR_NOP;
        csr_addr <= immediate_i(11 downto 0) when (opcode_type_i = OP_SYSTEM) else (others => '0');

        csr_rw_p : process(funct3_i, rd_addr_i, r1_addr)
        begin
            csr_op_i <= CSR_NOP;
            csr_rd_wen <= '0';
            case funct3_i is
                when CSRRW_T|CSRRWI_T =>
                    if rd_addr_i = "00000" then
                        csr_op_i <= CSRRW_WR;
                    else
                        csr_op_i <= CSRRW;
                        csr_rd_wen <= '1';
                    end if;
                when CSRRS_T|CSRRSI_T =>
                    if r1_addr = "00000" then
                        csr_op_i <= CSRRS_RD;
                        csr_rd_wen <= '1';
                    else
                        csr_op_i <= CSRRS;
                        csr_rd_wen <= '1';
                    end if;
                when CSRRC_T|CSRRCI_T =>
                    if r1_addr = "00000" then
                        csr_op_i <= CSRRC_RD;
                        csr_rd_wen <= '1';
                    else
                        csr_op_i <= CSRRC;
                        csr_rd_wen <= '1';
                    end if;
                when others =>
            end case;
        end process;
    end block csr_decode_block;
-- #################################

end behave;
