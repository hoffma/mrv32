-- ############################################################################
-- ################ REGISTER FILE DESCRIPTION #################################
-- ############################################################################
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regfile is
    Generic (
        REG_WIDTH : natural := 32;
        REG_COUNT : natural := 32
    );
    Port (
        clk     : in std_logic;
        r1_addr : in std_logic_vector(4 downto 0);
        r2_addr : in std_logic_vector(4 downto 0);
        wr_addr : in std_logic_vector(4 downto 0);
        wr_data : in std_logic_vector(31 downto 0);
        wr_en   : in std_Logic;
        r1_data : out std_logic_vector(31 downto 0);
        r2_data : out std_logic_vector(31 downto 0)
    );
end regfile;

architecture reg_gen of regfile is
    type reg_mux_t is array(0 to REG_COUNT-1) of std_logic_vector(REG_WIDTH-1 downto 0);

    signal reg_d : reg_mux_t;
    signal reg_q : reg_mux_t;
    signal reg_en : std_logic_vector(REG_COUNT-1 downto 0);
begin

    process(r1_addr, r2_addr, wr_addr, wr_data, wr_en, reg_q)
        variable rs1_addr_i : integer;
        variable rs2_addr_i : integer;
        variable rd_addr_i : integer;
    begin
        rs1_addr_i := to_integer(unsigned(r1_addr));
        rs2_addr_i := to_integer(unsigned(r2_addr));
        rd_addr_i := to_integer(unsigned(wr_addr));

        -- select the correct write port
        reg_en <= (others => '0');
        reg_d <= (others => (others => '0'));
        reg_d(rd_addr_i) <= wr_data;
        reg_en(rd_addr_i) <= wr_en;

        -- simple read port demux. r0 is always zero, so ignore the register.
        -- (Synthesis will hopefully notice this)
        r1_data <= (others => '0');
        r2_data <= (others => '0');
        if rs1_addr_i /= 0 then
            r1_data <= reg_q(rs1_addr_i);
        end if;
        if rs2_addr_i /= 0 then
            r2_data <= reg_q(rs2_addr_i);
        end if;
    end process;

    reg_gen : for I in 0 to REG_COUNT-1 generate
        reg_I : entity work.reg
        generic map (
            REG_WIDTH => REG_WIDTH
        ) port map (
            clk => clk,
            en => reg_en(I),
            d => reg_d(I),
            q => reg_q(I)
        );
    end generate;
end reg_gen;
