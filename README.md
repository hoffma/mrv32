 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Mario's RISC-V CPU - mrv32

Trying to build a rather simple RISC-V CPU and finding my way through processor
design. Currently the CPU runs the RV32i ISA with CSR extension and implements
has a very simple native interface that separates instruction and data access.
Currently they are never accessed at the same time, but maybe I'll try myself
with pipelining soon (tm).

The entity descriptions looks the following:

```vhdl
entity mrv32 is
    Generic (
        RESET_VECTOR : std_logic_vector(31 downto 0) := x"80000000"
    );
    Port (
        clk         : in STD_LOGIC;
        rst         : in STD_LOGIC;

        i_strb      : out std_logic;
        i_addr      : out std_logic_vector(31 downto 0);
        i_rdata     : in std_logic_vector(31 downto 0);
        i_valid     : in std_logic;

        d_strb      : out std_logic;
        d_wen       : out std_logic_vector(3 downto 0);
        d_addr      : out std_logic_vector(31 downto 0);
        d_wdata     : out std_logic_vector(31 downto 0);
        d_rdata     : in std_logic_vector(31 downto 0);
        d_valid     : in std_logic;

        irq          : in STD_LOGIC
    );
end mrv32;
```

- hdl/mrv32.vhd - This is the top level entity facing to the outside and memory
  maps the CSR entity at 0xffff_f000
- hdl/core/mrv32_core.vhd - This is the absolute minimal core, implementing the
  same interface. It just differes in with the interrupt lines. Between irq_ext
  and irq_tim for external and timer interrupts.


  # LICENSE

  [MIT License](https://files.catbox.moe/wrjabz.mp3)
