library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library std;
use std.env.finish;
use std.textio.all;

entity riscof_tb is
  generic (
    MEM_FILE : string;
    MEM_SIZE_WORDS : natural := ((2*1024*1024)/4)
    -- MEM_SIZE_BYTES : natural := (2*1024*1024) -- 2 MiB is the maximum stack size, apparently
  );
end riscof_tb;

architecture Behavioral of riscof_tb is
    -- mem size in words
    constant MEM_SIZE_C : natural := (MEM_SIZE_WORDS);

    -- type mem32_t is array(natural range<>) of std_logic_vector(31 downto 0);
    type mem8_t is array(natural range<>) of std_logic_vector(7 downto 0);

    impure function mem_init_f(f_name : string; byte_sel : natural) return mem8_t is
        file text_f : text open read_mode is f_name;
        variable text_line_v : line;
        variable mem_v : mem8_t(0 to (MEM_SIZE_C-1));
        variable idx_v : natural;
        variable word_v : bit_vector(31 downto 0);
    begin
        mem_v := (others => (others => '0'));
        idx_v := 0;

        while (endfile(text_f) = false) and (idx_v < MEM_SIZE_C) loop
            readline(text_f, text_line_v);
            hread(text_line_v, word_v);
            case byte_sel is
                when 0 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(7 downto 0));
                when 1 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(15 downto 8));
                when 2 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(23 downto 16));
                when 3 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(31 downto 24));
                when others =>
                    return mem_v;
            end case;
            idx_v := idx_v + 1;
        end loop;
        return mem_v;
    end function mem_init_f;

    signal clk, rst : std_logic := '0';

    type cpu_simple is record
        strb      : std_logic;
        wen       : std_logic_vector(3 downto 0);
        addr      : std_logic_vector(31 downto 0);
        wdata     : std_logic_vector(31 downto 0);
        rdata     : std_logic_vector(31 downto 0);
        valid     : std_logic;
    end record cpu_simple;
    signal s_cpu : cpu_simple;

    signal mem_b0 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 0);
    signal mem_b1 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 1);
    signal mem_b2 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 2);
    signal mem_b3 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 3);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    mem: process(rst, clk)
        variable addr_i : integer range 0 to (MEM_SIZE_C-1);
        variable mem_word : std_logic_vector(31 downto 0);
    begin
        if rst = '1' then
            s_cpu.rdata <= (others => '0');
            s_cpu.valid <= '0';
        elsif rising_edge(clk) then
            s_cpu.rdata <= (others => '0');
            s_cpu.valid <= s_cpu.strb;

            addr_i := to_integer(unsigned(s_cpu.addr(23 downto 2)));

            if s_cpu.strb = '1' then
                if s_cpu.addr(31 downto 24) = x"80" then
                    if s_cpu.wen(0) = '1' then
                        mem_b0(addr_i) <= s_cpu.wdata(07 downto 00);
                    end if;
                    if s_cpu.wen(1) = '1' then
                        mem_b1(addr_i) <= s_cpu.wdata(15 downto 08);
                    end if;
                    if s_cpu.wen(2) = '1' then
                        mem_b2(addr_i) <= s_cpu.wdata(23 downto 16);
                    end if;
                    if s_cpu.wen(3) = '1' then
                        mem_b3(addr_i) <= s_cpu.wdata(31 downto 24);
                    end if;
                    mem_word := mem_b3(addr_i) &
                                mem_b2(addr_i) &
                                mem_b1(addr_i) &
                                mem_b0(addr_i);
                    s_cpu.rdata <= mem_word;
                end if;
            end if;
        end if;
    end process;

    inst_proc : entity work.mrv32
    port map (
        clk => clk,
        rst => rst,

        strb => s_cpu.strb,
        wen => s_cpu.wen,
        addr => s_cpu.addr,
        wdata => s_cpu.wdata,
        rdata => s_cpu.rdata,
        valid => s_cpu.valid,

        int_tim => '0',
        int_ext => '0'
    );

    -- ##################### Simulation Triggers ##############################
    sim_triggers: process(clk)
    begin
        if rising_edge(clk) then
            if (s_cpu.strb = '1') and (s_cpu.wen /= "0000") and (s_cpu.addr = x"01000000") then
                case s_cpu.wdata is
                    when x"CAFECAFE" => -- end simulation
                        report "Finishing simulation." severity note;
                        finish;
                    when others =>
                        report "Simulation failed. Received: 0x" & to_hstring(s_cpu.wdata) severity error;
                        finish;
                end case;
            end if;
        end if;
    end process sim_triggers;


    -- ########################## Signature Dump ##############################
    signature_dump: process(clk)
        file     dump_file : text open write_mode is "mrv32-tmp.signature";
        variable line_v    : line;
        variable prev_strb : std_logic := '0';
    begin
        if rising_edge(clk) then
            if (s_cpu.strb = '1') and (prev_strb = '0') and 
            (s_cpu.wen /= "0000") and (s_cpu.addr = x"01000004") then
                -- report "Writing signature: 0x" & to_hstring(s_cpu.mem_wdata) severity note;
                write(line_v, to_hstring(s_cpu.wdata), right, 8);
                writeline(dump_file, line_v);
            end if;

            prev_strb := s_cpu.strb;
        end if;
    end process signature_dump;
end Behavioral;
