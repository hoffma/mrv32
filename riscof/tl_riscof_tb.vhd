library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tilelink.all;

library std;
use std.textio.all;

entity tb_sram is
  generic (
    MEM_FILE : string;
    MEM_SIZE_WORDS : natural := ((2*1024*1024)/4)
    -- MEM_SIZE_BYTES : natural := (2*1024*1024) -- 2 MiB is the maximum stack size, apparently
  );
  Port (
    clka : in std_logic;
    rsta : in std_logic;
    
    tl_a_a : in tlul_a_t;
    a_ready_a : out std_logic;
    tl_d_a : out tlul_d_t;
    d_ready_a : in std_logic;

    clkb : in std_logic;
    rstb : in std_logic;
    tl_a_b : in tlul_a_t;
    a_ready_b : out std_logic;
    tl_d_b : out tlul_d_t;
    d_ready_b : in std_logic
  );
  -- Port (
  --   clk : in std_logic;
  --   rst : in std_logic;
  --   -- channel A (master to slave)
  --   tl_a   : in tlul_m2s_t;
  --   a_ready : out std_logic;
  --   -- channel D (slave to master)
  --   tl_d  : out tlul_s2m_t;
  --   d_ready : in std_logic
  -- );
end tb_sram;

architecture behave of tb_sram is
    -- mem size in words
    constant MEM_SIZE_C : natural := (MEM_SIZE_WORDS);

    -- type mem32_t is array(natural range<>) of std_logic_vector(31 downto 0);
    type mem8_t is array(natural range<>) of std_logic_vector(7 downto 0);

    impure function mem_init_f(f_name : string; byte_sel : natural) return mem8_t is
        file text_f : text open read_mode is f_name;
        variable text_line_v : line;
        variable mem_v : mem8_t(0 to (MEM_SIZE_C-1));
        variable idx_v : natural;
        variable word_v : bit_vector(31 downto 0);
    begin
        mem_v := (others => (others => '0'));
        idx_v := 0;

        while (endfile(text_f) = false) and (idx_v < MEM_SIZE_C) loop
            readline(text_f, text_line_v);
            hread(text_line_v, word_v);
            case byte_sel is
                when 0 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(7 downto 0));
                when 1 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(15 downto 8));
                when 2 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(23 downto 16));
                when 3 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(31 downto 24));
                when others =>
                    return mem_v;
            end case;
            idx_v := idx_v + 1;
        end loop;
        return mem_v;
    end function mem_init_f;

    signal mem_b0 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 0);
    signal mem_b1 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 1);
    signal mem_b2 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 2);
    signal mem_b3 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 3);

    signal addr_a_i, addr_b_i : unsigned(23 downto 2);
    signal dout_a_i, dout_b_i: std_logic_vector(31 downto 0);
begin
  -- PORT A
  addr_a_i <= unsigned(tl_a_a.address(23 downto 2)); -- align address
  tl_d_a.param <= (others => 'U'); -- reseved to TL-C
  -- In TL-UL, d_size cannot be larger than the width of the physical data bus.
  -- log2(data_width_bytes)
  tl_d_a.size <= "10";
  tl_d_a.sink <= "0"; -- d_sink gets ignored in TL_UL
  tl_d_a.corrupt <= '0';
  tl_d_a.data <= dout_a_i when (d_ready_a = '1') else (others => 'U');

  a_ready_a <= '0' when rsta = '1' else '1';
  aport_p: process(clka)
      variable mem_word : std_logic_vector(31 downto 0);
  begin
      if rising_edge(clka) then
          -- default values
          tl_d_a.valid <= '0';
          tl_d_a.denied <= '0'; -- request is always processed
          tl_d_a.source <= (others => 'U');
          tl_d_a.opcode <= (others => 'U');
          dout_a_i <= (others => 'U');

          if tl_a_a.valid = '1' then
            case tl_a_a.opcode is
              -- TODO: care about a_mask
              when OP_GET =>
                report "[a bus] OP_GET";
                tl_d_a.valid <= '1';
                mem_word := mem_b3(to_integer(addr_a_i)) &
                            mem_b2(to_integer(addr_a_i)) &
                            mem_b1(to_integer(addr_a_i)) &
                            mem_b0(to_integer(addr_a_i));
                dout_a_i <= mem_word;
                tl_d_a.opcode <= OP_ACCESS_ACK_DATA;
                tl_d_a.source <= tl_a_a.source;
              -- when OP_GET

              when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA =>
                report "[a bus] OP_PUT";
                -- no writes allowed
                tl_d_a.valid <= '1';
                tl_d_a.source <= tl_a_a.source;
                tl_d_a.opcode <= OP_ACCESS_ACK;
              -- when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA

              when others =>
                tl_d_a.corrupt <= '1'; -- ??????
            end case;
          end if;
      end if;
  end process;

  -- PORT B
  addr_b_i <= unsigned(tl_a_b.address(23 downto 2)); -- align address

  tl_d_b.param <= (others => 'U'); -- reseved to TL-C
  -- In TL-UL, d_size cannot be larger than the width of the physical data bus.
  -- log2(data_width_bytes)
  tl_d_b.size <= "10";
  tl_d_b.sink <= "0"; -- d_sink gets ignored in TL_UL
  tl_d_b.corrupt <= '0';
  tl_d_b.data <= dout_b_i when (d_ready_b = '1') else (others => 'U');

  a_ready_b <= '0' when rstb = '1' else '1';

  bport_p: process(clkb)
      variable mem_word : std_logic_vector(31 downto 0);
  begin
    if rising_edge(clkb) then
      -- default values
      tl_d_b.valid <= '0';
      tl_d_b.denied <= '0'; -- request is always processed
      tl_d_b.source <= (others => 'U');
      tl_d_b.opcode <= (others => 'U');
      dout_b_i <= (others => 'U');

      if tl_a_b.valid = '1' then
        case tl_a_b.opcode is
          -- TODO: care about a_mask
          when OP_GET =>
            report "[b bus] OP_GET";
            tl_d_b.valid <= '1';
            mem_word := mem_b3(to_integer(addr_b_i)) &
                        mem_b2(to_integer(addr_b_i)) &
                        mem_b1(to_integer(addr_b_i)) &
                        mem_b0(to_integer(addr_b_i));
            dout_b_i <= mem_word;
            tl_d_b.opcode <= OP_ACCESS_ACK_DATA;
            tl_d_b.source <= tl_a_b.source;
          -- when OP_GET

          when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA =>
            report "[b bus] OP_PUT";
            if tl_a_b.mask(0) = '1' then
               mem_b0(to_integer(addr_b_i)) <= tl_a_b.data(7 downto 0);
            end if;
            if tl_a_b.mask(1) = '1' then
               mem_b1(to_integer(addr_b_i)) <= tl_a_b.data(15 downto 8);
            end if;
            if tl_a_b.mask(2) = '1' then
               mem_b2(to_integer(addr_b_i)) <= tl_a_b.data(23 downto 16);
            end if;
            if tl_a_b.mask(3) = '1' then
               mem_b3(to_integer(addr_b_i)) <= tl_a_b.data(31 downto 24);
            end if;

            tl_d_b.valid <= '1';
            tl_d_b.source <= tl_a_b.source;
            tl_d_b.opcode <= OP_ACCESS_ACK;
          -- when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA

          when others =>
            tl_d_b.corrupt <= '1'; -- ??????
        end case;
      end if;
    end if;
  end process;
end behave;



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.tilelink.all;

library std;
use std.env.finish;
use std.textio.all;

entity tl_riscof_tb is
  generic (
    MEM_FILE : string;
    MEM_SIZE_WORDS : natural := ((2*1024*1024)/4)
    -- MEM_SIZE_BYTES : natural := (2*1024*1024) -- 2 MiB is the maximum stack size, apparently
  );
end tl_riscof_tb;

architecture Behavioral of tl_riscof_tb is
    type bus_interface_t is record
        tl_a : tlul_a_t;
        a_ready : std_logic;
        tl_d : tlul_d_t;
        d_ready : std_logic;
    end record bus_interface_t;

    signal clk, rst : std_logic := '0';
    signal mrv_ibus_i, mrv_dbus_i: bus_interface_t;
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    inst_mrv32: entity work.mrv32_tl
    port map (
        clk => clk,
        rst => rst,

        ibus_tla => mrv_ibus_i.tl_a,
        ibus_a_ready => mrv_ibus_i.a_ready,
        ibus_tld => mrv_ibus_i.tl_d,
        ibus_d_ready => mrv_ibus_i.d_ready,

        dbus_tla => mrv_dbus_i.tl_a,
        dbus_a_ready => mrv_dbus_i.a_ready,
        dbus_tld => mrv_dbus_i.tl_d,
        dbus_d_ready => mrv_dbus_i.d_ready,

        int_tim => '0',
        int_ext => '0'
    );

    inst_sram : entity work.tb_sram
    generic map (
        MEM_FILE => MEM_FILE,
        MEM_SIZE_WORDS => MEM_SIZE_WORDS
    ) port map (
        clka => clk,
        rsta => rst,
        tl_a_a => mrv_ibus_i.tl_a,
        a_ready_a => mrv_ibus_i.a_ready,
        tl_d_a => mrv_ibus_i.tl_d,
        d_ready_a => mrv_ibus_i.d_ready,

        clkb => clk,
        rstb => rst,
        tl_a_b => mrv_dbus_i.tl_a,
        a_ready_b => mrv_dbus_i.a_ready,
        tl_d_b => mrv_dbus_i.tl_d,
        d_ready_b => mrv_dbus_i.d_ready
    );

    -- ##################### Simulation Triggers ##############################
    sim_triggers: process(clk)
    begin
        if rising_edge(clk) then
            if (mrv_dbus_i.tl_a.valid = '1') and (mrv_dbus_i.tl_a.mask /= "0000") 
                and (mrv_dbus_i.tl_a.address = x"01000000") then

                case mrv_dbus_i.tl_a.data is
                    when x"CAFECAFE" => -- end simulation
                        report "Finishing simulation." severity note;
                        finish;
                    when others =>
                        report "Simulation failed. Received: 0x" & to_hstring(mrv_dbus_i.tl_a.data) severity error;
                        finish;
                end case;
            end if;
        end if;
    end process sim_triggers;


    -- ########################## Signature Dump ##############################
    signature_dump: process(clk)
        file     dump_file : text open write_mode is "mrv32-tmp.signature";
        variable line_v    : line;
        variable prev_strb : std_logic := '0';
    begin
        if rising_edge(clk) then
            if (mrv_dbus_i.tl_a.valid = '1') and (prev_strb = '0') and 
            (mrv_dbus_i.tl_a.mask /= "0000") and (mrv_dbus_i.tl_a.address = x"01000004") then
                -- report "Writing signature: 0x" & to_hstring(s_cpu.mem_wdata) severity note;
                write(line_v, to_hstring(mrv_dbus_i.tl_a.data), right, 8);
                writeline(dump_file, line_v);
            end if;

            prev_strb := mrv_dbus_i.tl_a.valid;
        end if;
    end process signature_dump;
end Behavioral;
