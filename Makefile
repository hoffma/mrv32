.PHONY: all

TOPMOD := top
TOPFILE := hdl/$(TOPMOD).vhd
SIMMOD := $(TOPMOD)_tb

VHDL_STD := 08

MEMFILE := sim_sw/main.hex
MEMSIZE = $(shell wc -l < $(MEMFILE))
# RUN_FLAGS = -gMEM_FILE=$(MEMFILE) -gMEM_SIZE_WORDS=$(MEMSIZE) --max-stack-alloc=0 --stop-time=10us
RUN_FLAGS = 

ADDFILES :=
ADDFILES += hdl/core/constants.vhd
ADDFILES += hdl/core/types.vhd

ADDFILES += hdl/core/reg.vhd
ADDFILES += hdl/core/regfile.vhd

ADDFILES += hdl/core/barrel_shifter.vhd
ADDFILES += hdl/core/alu.vhd

ADDFILES += hdl/core/csr.vhd

ADDFILES += hdl/core/idecode.vhd
ADDFILES += hdl/core/control_unit.vhd

ADDFILES += hdl/core/mrv32_core.vhd

ADDFILES += hdl/tilelink.vhd
ADDFILES += hdl/mrv32.vhd
ADDFILES += hdl/mrv32_tl.vhd
ADDFILES += hdl/tl_sram.vhd

TBFILE := tb/$(SIMMOD).vhd
SIMFILE := $(TOPMOD)_sim.ghw

all: $(SIMFILE)

sim: $(SIMFILE)

$(SIMFILE): $(TOPFILE) $(ADDFILES) $(TBFILE) $(MEMFILE)
	mkdir -p build
	ghdl -a --workdir=build --std=$(VHDL_STD) $(ADDFILES) 
	ghdl -a --workdir=build --std=$(VHDL_STD) $(TOPFILE)
	ghdl -a --workdir=build --std=$(VHDL_STD) $(TBFILE)
	ghdl -e --workdir=build --std=$(VHDL_STD) $(SIMMOD)
	# ghdl -r --workdir=build --std=$(VHDL_STD) $(SIMMOD) --stop-time=5us
	# ghdl -r --workdir=build --std=$(VHDL_STD) $(SIMMOD) --stop-time=5us --wave=$(SIMFILE)
	ghdl -r --workdir=build --std=$(VHDL_STD) $(SIMMOD) $(RUN_FLAGS) --wave=$(SIMFILE)

.PHONY: clean
clean:
	rm -f *.log
	rm -f *.o
	rm -f *.ghk
	rm -f *.ghw
	rm -f *.cf
	rm -f $(SIMMOD)
	rm -rf build/
	rm -rf vunit_out/

