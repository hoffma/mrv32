library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.types.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity alu_tb is
  generic (runner_cfg : string);
end alu_tb;

architecture behave of alu_tb is
    signal clk, rst : std_logic := '0';

    signal alu_a, alu_b, alu_y : std_logic_vector(31 downto 0);
    signal alu_op : aluop_t;
    signal alu_busy, alu_lt, alu_ltu, alu_eq : std_logic;
begin

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 45 ns;

    stim: process
        variable curr_op : aluop_t;
    begin
        test_runner_setup(runner, runner_cfg);
        report "Hello World";
        for I in aluop_t'left to aluop_t'right  loop
            report "i: " & aluop_t'image(i);
            wait until rising_edge(clk);
        end loop;
        test_runner_cleanup(runner);

    end process;

    inst_alu: entity work.alu
    port map (
        a => alu_a,
        b => alu_b,
        alu_op => alu_op,
        busy => alu_busy,
        y => alu_y,
        LT => alu_lt,
        LTU => alu_ltu,
        EQ => alu_eq
    );
end behave;

