#!/usr/bin/env bash

XLEN=32

if [[ $ELF_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_gcc.sh]: ELF_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "ELF_INSTALL is: ${ELF_INSTALL}"
fi

mkdir -p ${ELF_INSTALL}

git clone https://github.com/sifive/elf2hex.git
cd elf2hex
autoreconf -i
./configure --prefix=${ELF_INSTALL} --target=riscv${XLEN}-unknown-elf
make
make install
