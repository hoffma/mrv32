# Toolchain building scripts

Just a simple collection to build needed toolchains, especially for the
compliance test.

A thourough test was not made, but the requirements should be installed with
the ```setup_requirements.sh``` script.

The build scripts always need a variable for the installation path. A few
examples to install it locally are given below:

Example:
```
GCC_INSTALL=$(pwd)/riscv32 ./build_gcc.sh
GHDL_INSTALL=$(pwd)/ghdl ./build_gcc.sh
SAIL_INSTALL=$(pwd)/ghdl ./build_gcc.sh
SPIKE_INSTALL=$(pwd)/ghdl ./build_spike.sh
```

TODO: Maybe make this easier or a generic variable for a base-path
