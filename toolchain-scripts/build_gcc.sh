#!/bin/bash

BUILD_ARCH="rv32imac"
BUILD_ABI="ilp32"
# BUILD_ARCH="rv32gc"
# BUILD_ABI="ilp32d"
# for pure rv32i
# BUILD_ARCH="rv32i"
# BUILD_ABI="ilp32"

function yes_or_no {
    while true; do
        read -p "$* [Y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;  
            [Nn]*) echo "Aborted" ; return  1 ;;
	    *) return 0 ;;
        esac
    done
}

if [[ $GCC_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_gcc.sh]: GCC_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "GCC_INSTALL is: ${GCC_INSTALL}"
  if [ $(yes_or_no) ]; 
  then
    echo "Aborting. Bye."
    exit 1
  else
    echo "Starting GCC install."
  fi

fi

# source: https://github.com/riscv-collab/riscv-gnu-toolchain
# prerequisites
sudo apt-get install -y autoconf automake autotools-dev curl python3 python3-pip \
  libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo \
  gperf libtool patchutils bc zlib1g-dev libexpat-dev ninja-build git cmake \
  libglib2.0-dev
mkdir -p $GCC_INSTALL

git clone --recursive https://github.com/riscv/riscv-gnu-toolchain

cd riscv-gnu-toolchain

./configure --prefix=$GCC_INSTALL --with-arch=${BUILD_ARCH} --with-abi=${BUILD_ABI}
make -j$(nproc) # build with newlib
make install
