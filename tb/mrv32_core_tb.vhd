library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library std;
use std.textio.all;
use std.env.finish;

entity mrv32_core_tb is
  generic (
    MEM_FILE : string;
    MEM_SIZE_WORDS : natural := ((2*1024*1024)/4)
  );
end mrv32_core_tb;

architecture Behavioral of mrv32_core_tb is
    signal clk, rst : std_logic := '0';
    -- mem size in words
    -- constant MEM_SIZE_C : natural := (MEM_SIZE_WORDS);
    constant MEM_SIZE_C : natural := 16#2000#;
    type mem8_t is array(natural range<>) of std_logic_vector(7 downto 0);

    impure function mem_init_f(f_name : string; byte_sel : natural) return mem8_t is
        file text_f : text open read_mode is f_name;
        variable text_line_v : line;
        variable mem_v : mem8_t(0 to (MEM_SIZE_C-1));
        variable idx_v : natural;
        variable word_v : bit_vector(31 downto 0);
    begin
        mem_v := (others => (others => '0'));
        idx_v := 0;

        while (endfile(text_f) = false) and (idx_v < MEM_SIZE_C) loop
            readline(text_f, text_line_v);
            hread(text_line_v, word_v);
            case byte_sel is
                when 0 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(7 downto 0));
                when 1 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(15 downto 8));
                when 2 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(23 downto 16));
                when 3 =>
                    mem_v(idx_v) := to_stdlogicvector(word_v(31 downto 24));
                when others =>
                    return mem_v;
            end case;
            idx_v := idx_v + 1;
        end loop;
        return mem_v;
    end function mem_init_f;
    
    type cpu_interface is record
        i_strb      : std_logic;
        i_addr      : std_logic_vector(31 downto 0);
        i_rdata     : std_logic_vector(31 downto 0);
        i_valid     : std_logic;

        d_strb      : std_logic;
        d_wen       : std_logic_vector(3 downto 0);
        d_addr      : std_logic_vector(31 downto 0);
        d_wdata     : std_logic_vector(31 downto 0);
        d_rdata     : std_logic_vector(31 downto 0);
        d_valid     : std_logic;
    end record cpu_interface;

    signal s_cpu : cpu_interface;

    signal tmp_reg : std_logic_vector(31 downto 0);
    signal cpu_irq : std_logic := '0';

    signal mem_b0 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 0);
    signal mem_b1 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 1);
    signal mem_b2 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 2);
    signal mem_b3 : mem8_t(0 to (MEM_SIZE_C-1)) := mem_init_f(MEM_FILE, 3);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    process
    begin
        wait until rst = '0';
        loop
            cpu_irq <= '0';
            wait for 2 us; -- wait some time between interrupts
            cpu_irq <= '1';
            wait until rising_edge(clk);
        end loop;
    end process;

    imem : process(rst, clk)
        variable addr_i : integer range 0 to (MEM_SIZE_C-1);
        variable mem_word : std_logic_vector(31 downto 0);
    begin
        if rst = '1' then
            s_cpu.i_rdata <= (others => '0');
            s_cpu.i_valid <= '0';
        elsif rising_edge(clk) then
            s_cpu.i_rdata <= (others => '0');
            s_cpu.i_valid <= s_cpu.i_strb;

            addr_i := to_integer(unsigned(s_cpu.i_addr(23 downto 2)));

            if s_cpu.i_strb = '1' then
                if s_cpu.i_addr(31 downto 24) = x"80" then
                    mem_word := mem_b3(addr_i) &
                                mem_b2(addr_i) &
                                mem_b1(addr_i) &
                                mem_b0(addr_i);
                    s_cpu.i_rdata <= mem_word;
                else
                    s_cpu.i_rdata <= (others => 'U');
                end if;
            end if;
        end if;
    end process;

    dmem: process(rst, clk)
        variable addr_i : integer range 0 to (MEM_SIZE_C-1);
        variable mem_word : std_logic_vector(31 downto 0);
    begin
        if rst = '1' then
            s_cpu.d_rdata <= (others => '0');
            s_cpu.d_valid <= '0';
            tmp_reg <= (others => '0');
        elsif rising_edge(clk) then
            s_cpu.d_rdata <= (others => '0');
            s_cpu.d_valid <= s_cpu.d_strb;

            addr_i := to_integer(unsigned(s_cpu.d_addr(23 downto 2)));

            if tmp_reg = x"0000_0004" then
                report "Simulation finished";
                finish;
            end if;

            if s_cpu.d_strb = '1' then
                if s_cpu.d_addr(31 downto 24) = x"80" then
                    if s_cpu.d_wen(0) = '1' then
                        mem_b0(addr_i) <= s_cpu.d_wdata(07 downto 00);
                    end if;
                    if s_cpu.d_wen(1) = '1' then
                        mem_b1(addr_i) <= s_cpu.d_wdata(15 downto 08);
                    end if;
                    if s_cpu.d_wen(2) = '1' then
                        mem_b2(addr_i) <= s_cpu.d_wdata(23 downto 16);
                    end if;
                    if s_cpu.d_wen(3) = '1' then
                        mem_b3(addr_i) <= s_cpu.d_wdata(31 downto 24);
                    end if;
                    mem_word := mem_b3(addr_i) &
                                mem_b2(addr_i) &
                                mem_b1(addr_i) &
                                mem_b0(addr_i);
                    s_cpu.d_rdata <= mem_word;
                elsif s_cpu.d_addr(31 downto 24) = x"01" then
                    s_cpu.d_rdata <= (others => '0'); 
                    if s_cpu.d_wen /= "0000" then
                        tmp_reg <= s_cpu.d_wdata;
                    else
                        s_cpu.d_rdata <= tmp_reg; 
                    end if;
                else
                    s_cpu.d_rdata <= (others => '0');
                end if;
            end if;
        end if;
    end process;
    
    inst_proc : entity work.mrv32_core
    generic map (
        RESET_VECTOR => x"8000_0000"
    )
    port map (
        clk => clk,
        rst => rst,
        i_strb => s_cpu.i_strb,
        i_addr => s_cpu.i_addr,
        i_rdata => s_cpu.i_rdata,
        i_valid => s_cpu.i_valid,

        d_strb => s_cpu.d_strb,
        d_wen => s_cpu.d_wen,
        d_addr => s_cpu.d_addr,
        d_wdata => s_cpu.d_wdata,
        d_rdata => s_cpu.d_rdata,
        d_valid => s_cpu.d_valid,

        int_tim => '0',
        int_ext => cpu_irq
    );
end Behavioral;
