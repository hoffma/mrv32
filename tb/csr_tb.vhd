library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.types.all;

library std;
use std.env.finish;

entity csr_tb is
end csr_tb;

architecture behave of csr_tb is
    
    signal clk, rst : std_logic := '0';

    signal addr : std_logic_vector(11 downto 0);
    signal din, dout : std_logic_vector(31 downto 0);
    signal csr_op : csr_instr_t;

    constant M_CYCLE : std_logic_vector(11 downto 0) := x"b00";
    constant M_CYCLEH : std_logic_vector(11 downto 0) := x"b80";
    constant U_CYCLE : std_logic_vector(11 downto 0) := x"c00";
    constant U_CYCLEH : std_logic_vector(11 downto 0) := x"c80";
begin

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    process
    begin
        din <= (others => '0');
        addr <= (others => '0');
        csr_op <= CSR_NOP;
        wait until rst = '0';
        wait until rising_edge(clk);
        -- start simulation

        csr_op <= CSRRW;
        addr <= U_CYCLE;
        din <= (others => '0');
        wait until rising_edge(clk);

        -- set M_CYCLE
        csr_op <= CSRRW_WR;
        addr <= M_CYCLE;
        din <= x"8000_0000";
        wait until rising_edge(clk);

        din <= (others => '0');
        addr <= (others => '0');
        csr_op <= CSR_NOP;
        wait until rising_edge(clk);

        -- wait a bit
        wait for 100 ns;
        wait until rising_edge(clk);

        -- read M_CYCLE with CSRRC
        csr_op <= CSRRC_RD;
        addr <= M_CYCLE;
        din <= (others => '0');
        wait until rising_edge(clk);

        csr_op <= CSRRS;
        addr <= M_CYCLE;
        din <= x"0000_1000";
        wait until rising_edge(clk);

        csr_op <= CSRRC;
        addr <= M_CYCLE;
        din <= x"0000_000f";
        wait until rising_edge(clk);

        din <= (others => '0');
        addr <= (others => '0');
        csr_op <= CSR_NOP;
        wait until rising_edge(clk);

        wait for 2 us;
        report "finished simulation";
        finish;
    end process;

    inst_csr : entity work.csr
    generic map (
        CLK_MHZ => 100,
        TIME_TICK_US => 1
    ) port map (
        clk => clk,
        rst => rst,
        op => csr_op,
        din => din,
        addr => addr,
        dout => dout
    );
end behave;
