library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity top_tb is
end top_tb;

architecture behave of top_tb is
    signal clk, rst : std_logic := '0';
    signal led : std_logic_vector(7 downto 0);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 45 ns;

    stim: process
    begin
        wait until rst = '0';
        report "simulation started";
        wait until led > x"05";
        report "simulation finished";
        finish;
    end process;

    inst_top: entity work.top
    port map (
        clk => clk,
        rst => rst,
        led => led
    );
end behave;
