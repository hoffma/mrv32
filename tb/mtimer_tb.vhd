library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity mtimer_tb is
end mtimer_tb;

architecture behave of mtimer_tb is
    signal clk, rst : std_logic := '0';

    signal en, wen : std_logic := '0';
    signal addr : std_logic_vector(2 downto 0) := (others => '0');
    signal din, dout : std_logic_vector(31 downto 0) := (others => '0');
    signal interrupt : std_logic := '0';
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;

    stim : process
    begin
        en <= '0';
        wen <= '0';
        addr <= "000";
        din <= (others => '0');
        wait until rst = '0';
        report "Rst low. Simulation started.";
        addr <= "100";
        din <= x"0000_0003";
        wen <= '1';
        wait until rising_edge(clk);
        addr <= "010";
        din <= x"0000_000a";
        wen <= '1';
        wait until rising_edge(clk);
        addr <= "000";
        din <= x"0000_0000";
        wen <= '1';
        wait until rising_edge(clk);
        wen <= '0';
        wait until rising_edge(clk);
        wait for 100 ns;
        en <= '1';
        wait until rising_edge(clk);
        wait until interrupt = '1';
        en <= '0';
        wait for 200 ns;

        report "Simulation finished";
        finish;
    end process;

    inst_mtimer : entity work.mtimer
    port map (
        clk => clk,
        rst => rst,
        en => en,
        wen => wen,
        addr => addr,
        din => din,
        dout => dout,
        int => interrupt
    );
end behave;
