library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tilelink.all;

entity top is
    Port (
        clk : in std_logic;
        rst : in std_logic;
        led : out std_logic_vector(7 downto 0)
    );
end top;

architecture behave of top is
    type bus_interface_t is record
        tl_a : tlul_a_t;
        a_ready : std_logic;
        tl_d : tlul_d_t;
        d_ready : std_logic;
    end record bus_interface_t;

    signal mrv_ibus_i, mrv_dbus_i, sram_i, led_i : bus_interface_t;
    signal tmp_bus_i : bus_interface_t;
    signal led_reg : std_logic_vector(31 downto 0) := (others => 'U');

    type selection_t is (SRAM_SEL, LED_SEL);
    signal s_sel : selection_t := SRAM_SEL;
begin
    led <= led_reg(7 downto 0);

    process(mrv_dbus_i, led_i, sram_i)
        variable addr_nibble : std_logic_vector(7 downto 0);
    begin
        addr_nibble := mrv_dbus_i.tl_a.address(31 downto 24);
        -- mrv_dbus_i.tl_d <= (others => (others => '0'));
        -- mrv_dbus_i.ready <= '0';
        -- led_i.tl_a <= (others => (others => '0'));
        -- led_i.d_ready <= '0';
        -- sram_i.tl_a <= (others => (others => '0'));
        -- sram_i.d_ready <= '0';
        mrv_dbus_i.tl_d.opcode <= (others => '0');
        mrv_dbus_i.tl_d.param <= (others => '0');
        mrv_dbus_i.tl_d.size <= (others => '0');
        mrv_dbus_i.tl_d.source <= (others => '0');
        mrv_dbus_i.tl_d.sink <= (others => '0');
        mrv_dbus_i.tl_d.denied <= '0';
        mrv_dbus_i.tl_d.data <= (others => '0');
        mrv_dbus_i.tl_d.corrupt <= '0';
        mrv_dbus_i.tl_d.valid <= '0';

        case addr_nibble is
            when x"01" =>
                led_i.tl_a <= mrv_dbus_i.tl_a;
                mrv_dbus_i.a_ready <= led_i.a_ready;
                mrv_dbus_i.tl_d <= led_i.tl_d;
                led_i.d_ready <= mrv_dbus_i.d_ready;
                s_sel <= LED_SEL;
            when others =>
                sram_i.tl_a <= mrv_dbus_i.tl_a;
                mrv_dbus_i.a_ready <= sram_i.a_ready;
                mrv_dbus_i.tl_d <= sram_i.tl_d;
                sram_i.d_ready <= mrv_dbus_i.d_ready;
                s_sel <= SRAM_SEL;
        end case;
    end process;

    led_p: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                led_reg <= (others => '0');
                led_i.a_ready <= '0';
            else
                led_i.a_ready <= '1';
                led_i.tl_d.valid <= '0';
                led_i.tl_d.data <= (others => '0');
                led_i.tl_d.opcode <= (others => '0');
                led_i.tl_d.source <= (others => '0');
                led_i.tl_d.denied <= '0';
                led_i.tl_d.corrupt <= '0';
                led_i.tl_d.size <= "10";
                led_i.tl_d.sink <= (others => '0');
                led_i.tl_d.param <= (others => 'U');

                if led_i.tl_a.valid = '1' then
                    case led_i.tl_a.opcode is
                        when OP_GET =>
                            led_i.tl_d.valid <= '1';
                            led_i.tl_d.data <= led_reg;
                            led_i.tl_d.opcode <= OP_ACCESS_ACK_DATA;
                            led_i.tl_d.source <= led_i.tl_a.source;
                        when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA =>
                            if led_i.tl_a.mask(0) = '1' then
                                led_reg <= led_i.tl_a.data;
                            end if;
                            led_i.tl_d.valid <= '1';
                            led_i.tl_d.opcode <= OP_ACCESS_ACK;
                            led_i.tl_d.source <= led_i.tl_a.source;
                        when others =>
                    end case;
                end if;
            end if;
        end if;
    end process;

    inst_mrv32: entity work.mrv32_tl
    port map (
        clk => clk,
        rst => rst,

        ibus_tla => mrv_ibus_i.tl_a,
        ibus_a_ready => mrv_ibus_i.a_ready,
        ibus_tld => mrv_ibus_i.tl_d,
        ibus_d_ready => mrv_ibus_i.d_ready,

        dbus_tla => mrv_dbus_i.tl_a,
        dbus_a_ready => mrv_dbus_i.a_ready,
        dbus_tld => mrv_dbus_i.tl_d,
        dbus_d_ready => mrv_dbus_i.d_ready,

        int_tim => '0',
        int_ext => '0'
    );

    inst_sram : entity work.tl_sram
    port map (
        clka => clk,
        rsta => rst,
        tl_a_a => mrv_ibus_i.tl_a,
        a_ready_a => mrv_ibus_i.a_ready,
        tl_d_a => mrv_ibus_i.tl_d,
        d_ready_a => mrv_ibus_i.d_ready,

        clkb => clk,
        rstb => rst,
        tl_a_b => sram_i.tl_a,
        a_ready_b => sram_i.a_ready,
        tl_d_b => sram_i.tl_d,
        d_ready_b => sram_i.d_ready
    );
end behave;
