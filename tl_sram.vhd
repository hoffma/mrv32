-- MIT License
--
-- Copyright (c) 2024 Mario
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- tl;dr: https://files.catbox.moe/wrjabz.mp3
-- Auther: Mario Hoffmann

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tilelink.all;

entity tl_sram is
  Port (
    clka : in std_logic;
    rsta : in std_logic;
    
    tl_a_a : in tlul_a_t;
    a_ready_a : out std_logic;
    tl_d_a : out tlul_d_t;
    d_ready_a : in std_logic;

    clkb : in std_logic;
    rstb : in std_logic;
    tl_a_b : in tlul_a_t;
    a_ready_b : out std_logic;
    tl_d_b : out tlul_d_t;
    d_ready_b : in std_logic
  );
  -- Port (
  --   clk : in std_logic;
  --   rst : in std_logic;
  --   -- channel A (master to slave)
  --   tl_a   : in tlul_m2s_t;
  --   a_ready : out std_logic;
  --   -- channel D (slave to master)
  --   tl_d  : out tlul_s2m_t;
  --   d_ready : in std_logic
  -- );
end tl_sram;

architecture behave of tl_sram is
  type mem_t is array (natural range <>) of std_logic_vector(31 downto 0);
  signal mem_data : mem_t(0 to 4095) := (
x"010002b7",
x"00000013",
x"00000013",
x"00000013",
x"00200313",
x"0062a023",
x"0002a403",
x"00140413",
x"00000013",
x"00000013",
x"00000013",
x"00000013",
x"0082a023",
x"fe5ff06f",
    others => (others => '0')
  );

  signal addr_a_i, addr_b_i : unsigned(23 downto 2);
  signal dout_a_i, dout_b_i: std_logic_vector(31 downto 0);
begin
  -- PORT A
  addr_a_i <= unsigned(tl_a_a.address(23 downto 2)); -- align address
  tl_d_a.param <= (others => 'U'); -- reseved to TL-C
  -- In TL-UL, d_size cannot be larger than the width of the physical data bus.
  -- log2(data_width_bytes)
  tl_d_a.size <= "10";
  tl_d_a.sink <= "0"; -- d_sink gets ignored in TL_UL
  tl_d_a.corrupt <= '0';
  tl_d_a.data <= dout_a_i when (d_ready_a = '1') else (others => 'U');

  a_ready_a <= '0' when rsta = '1' else '1';
  aport_p: process(clka)
  begin
      if rising_edge(clka) then
          -- default values
          tl_d_a.valid <= '0';
          tl_d_a.denied <= '0'; -- request is always processed
          tl_d_a.source <= (others => 'U');
          tl_d_a.opcode <= (others => 'U');
          dout_a_i <= (others => 'U');

          if tl_a_a.valid = '1' then
            case tl_a_a.opcode is
              -- TODO: care about a_mask
              when OP_GET =>
                tl_d_a.valid <= '1';
                dout_a_i <= mem_data(to_integer(addr_a_i));
                tl_d_a.opcode <= OP_ACCESS_ACK_DATA;
                tl_d_a.source <= tl_a_a.source;
              -- when OP_GET

              when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA =>
                -- no writes allowed
                tl_d_a.valid <= '1';
                tl_d_a.source <= tl_a_a.source;
                tl_d_a.opcode <= OP_ACCESS_ACK;
              -- when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA

              when others =>
                tl_d_a.corrupt <= '1'; -- ??????
            end case;
          end if;
      end if;
  end process;

  -- PORT B
  addr_b_i <= unsigned(tl_a_b.address(23 downto 2)); -- align address

  tl_d_b.param <= (others => 'U'); -- reseved to TL-C
  -- In TL-UL, d_size cannot be larger than the width of the physical data bus.
  -- log2(data_width_bytes)
  tl_d_b.size <= "10";
  tl_d_b.sink <= "0"; -- d_sink gets ignored in TL_UL
  tl_d_b.corrupt <= '0';
  tl_d_b.data <= dout_b_i when (d_ready_b = '1') else (others => 'U');

  a_ready_b <= '0' when rstb = '1' else '1';

  bport_p: process(clkb)
  begin
    if rising_edge(clkb) then
      -- default values
      tl_d_b.valid <= '0';
      tl_d_b.denied <= '0'; -- request is always processed
      tl_d_b.source <= (others => 'U');
      tl_d_b.opcode <= (others => 'U');
      dout_b_i <= (others => 'U');

      if tl_a_b.valid = '1' then
        case tl_a_b.opcode is
          -- TODO: care about a_mask
          when OP_GET =>
            tl_d_b.valid <= '1';
            dout_b_i <= mem_data(to_integer(addr_b_i));
            tl_d_b.opcode <= OP_ACCESS_ACK_DATA;
            tl_d_b.source <= tl_a_b.source;
          -- when OP_GET

          when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA =>
            if tl_a_b.mask(0) = '1' then
               mem_data(to_integer(addr_b_i))(7 downto 0) <= tl_a_b.data(7 downto 0);
            end if;
            if tl_a_b.mask(1) = '1' then
               mem_data(to_integer(addr_b_i))(15 downto 8) <= tl_a_b.data(15 downto 8);
            end if;
            if tl_a_b.mask(2) = '1' then
               mem_data(to_integer(addr_b_i))(23 downto 16) <= tl_a_b.data(23 downto 16);
            end if;
            if tl_a_b.mask(3) = '1' then
               mem_data(to_integer(addr_b_i))(31 downto 24) <= tl_a_b.data(31 downto 24);
            end if;

            tl_d_b.valid <= '1';
            tl_d_b.source <= tl_a_b.source;
            tl_d_b.opcode <= OP_ACCESS_ACK;
          -- when OP_PUT_FULL_DATA | OP_PUT_PARTIAL_DATA

          when others =>
            tl_d_b.corrupt <= '1'; -- ??????
        end case;
      end if;
    end if;
  end process;
end behave;
