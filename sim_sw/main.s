
start:
auipc t1, 0x2
mv t1, t1
nop

init_Mcratch:
csrrw s0, mscratch, t1

init_Mtvec:
la s0, trap_handler
csrrw s0, mtvec, s0

# tmp_reg address
li t0, 0x01000000
nop
nop
nop
# clear tmp_reg
li t1, 0x02
sw t1, 0(t0)

# enable timer interrupts
# li a1, 128
# enable ext interrupts
li a1, 2048
csrw mie, a1
li a1, 8
csrw mstatus, a1

loop:
    nop
    j loop

trap_handler:
# increment tmp_reg
lw a1, 0(t0)
addi a1, a1, 1
sw a1, 0(t0)

sw zero, 20(t1)
sw zero, 4(t1)
sw zero, 0(t1)
li a1, 1
sw a1, 20(t1)
mret
